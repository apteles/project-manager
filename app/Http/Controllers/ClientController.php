<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ClientService;
use App\Repositories\ClientRepository;

class ClientController extends Controller
{
    /**
     * @var ClientRepository
     */
    private $client;

    /**
     * @var ClientService
     */
    private $clientService;

    public function __construct(ClientRepository $client, ClientService $clientService)
    {
        $this->client = $client;
        $this->clientService = $clientService;
    }

    public function index(ClientRepository $client)
    {
        return $client->all();
    }

    public function store(Request $request)
    {
        $created = $this->clientService->create($request->all());

        if (isset($created['error'])) {
            return response()->json($created['error_description'], 400);
        }
    }

    public function show(string $id)
    {
        return $this->client->find($id);
    }

    public function destroy(string $id)
    {
        $client = $this->client->find($id);
        if ($client->delete()) {
            return $client;
        }
    }
}
