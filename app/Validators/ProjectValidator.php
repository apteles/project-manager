<?php
declare(strict_types=1);
namespace App\Validators;

use Prettus\Validator\LaravelValidator;
use Prettus\Validator\Contracts\ValidatorInterface;

class ProjectValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'owner_id' => 'required',
            'client_id' =>'required',
            'name' =>'required',
            'progress' =>'required',
            'status' =>'required',
            'due_date' =>'required',
        ],
    ];
}
