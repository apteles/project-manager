<?php


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/clients', 'ClientController@index');
Route::get('/clients/{id}', 'ClientController@show');
Route::post('/clients', 'ClientController@store');
Route::delete('/clients/{id}', 'ClientController@destroy');


Route::resource('project', 'ProjectController', ['except' => ['create', 'edit']]);
Route::get('/project/{id}/notes', 'ProjectNoteController@index');
Route::post('/project/{id}/notes', 'ProjectNoteController@store');
Route::get('/project/{id}/notes/{noteID}', 'ProjectNoteController@show');
Route::put('/project/{id}/notes/{noteID}', 'ProjectNoteController@update');
Route::delete('/project/{id}/notes/{noteID}', 'ProjectNoteController@destroy');
