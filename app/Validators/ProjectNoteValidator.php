<?php

namespace App\Validators;

use \Prettus\Validator\LaravelValidator;
use \Prettus\Validator\Contracts\ValidatorInterface;

/**
 * Class ProjectNoteValidator.
 *
 * @package namespace App\Validators;
 */
class ProjectNoteValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'project_id' => 'required|integer',
            'title' =>'required',
            'note' =>'required',
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];
}
