<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Project;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Project::class, function (Faker $faker) {
    return [
        'owner_id' => \rand(1, 10),
        'client_id' => \rand(1, 10),
        'name' => $faker->word,
        'description' => $faker->sentence,
        'progress' => \rand(1, 100),
        'status' => \rand(1, 3),
        'due_date' => $faker->dateTime('now')

    ];
});
