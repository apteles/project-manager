<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Validators\ProjectNoteValidator;
use App\Repositories\ProjectNoteRepository;
use App\Http\Requests\ProjectNoteCreateRequest;
use App\Http\Requests\ProjectNoteUpdateRequest;

/**
 * Class ProjectNotesController.
 *
 * @package namespace App\Http\Controllers;
 */
class ProjectNoteController extends Controller
{
    /**
     * @var ProjectNoteRepository
     */
    protected $repository;

    /**
     * @var ProjectNoteValidator
     */
    protected $validator;

    /**
     * ProjectNotesController constructor.
     *
     * @param ProjectNoteRepository $repository
     * @param ProjectNoteValidator $validator
     */
    public function __construct(ProjectNoteRepository $repository, ProjectNoteValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(string $projectId)
    {
        return $this->repository->findWhere(['project_id' => $projectId]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProjectNoteCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(Request $request)
    {
        $projectNote = $this->repository->create($request->all());

        $response = ['data'=> $projectNote->toArray()];

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(string $id, string $noteID)
    {
        $projectNote = $this->repository->findWhere(['project_id' => $id, 'id' => $noteID]);

        return response()->json([
                'data' => $projectNote,
            ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  ProjectNoteUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(Request $request, $id)
    {
        $projectNote = $this->repository->update($request->all(), $id);

        $response = [
                'message' => 'ProjectNote updated.',
                'data'    => $projectNote->toArray(),
            ];
        return response()->json($response);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $noteID)
    {
        $deleted = $this->repository->delete($noteID);

        return response()->json([
                'message' => 'ProjectNote deleted.',
                'deleted' => $deleted,
            ]);
    }
}
