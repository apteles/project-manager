<?php

namespace App\Providers;

use App\Repositories\ClientRepository;
use App\Repositories\ProjectRepository;
use Illuminate\Support\ServiceProvider;

use App\Repositories\ProjectNoteRepository;
use App\Repositories\ClientRepositoryEloquent;
use App\Repositories\ProjectRepositoryEloquent;
use App\Repositories\ProjectNoteRepositoryEloquent;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(ClientRepository::class, ClientRepositoryEloquent::class);
        $this->app->bind(ProjectRepository::class, ProjectRepositoryEloquent::class);
        $this->app->bind(ProjectNoteRepository::class, ProjectNoteRepositoryEloquent::class);
    }
}
