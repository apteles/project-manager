<?php
declare(strict_types=1);
namespace App\Services;

use App\Validators\ProjectValidator;
use App\Validators\ProjectNoteValidator;
use App\Repositories\ProjectNoteRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class ProjectNoteService
{
    /**
     *
     * @var ProjectNoteRepository
     */
    private $projectNoteModel;

    /**
     *
     * @var ProjectValidator
     */
    private $projectNoteValidator;

    public function __construct(ProjectNoteRepository $model, ProjectNoteValidator $validator)
    {
        $this->projectModel = $model;
        $this->projectNoteValidator = $validator;
    }

    public function all()
    {
        return $this->projectModel->all();
    }

    public function find($id)
    {
        return $this->projectModel->find($id);
    }

    public function create(array $data)
    {
        try {
            $this->projectNoteValidator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
            return $this->projectModel->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }
}
