<?php
declare(strict_types=1);
namespace App\Services;

use App\Validators\ProjectValidator;
use App\Repositories\ProjectRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class ProjectService
{
    /**
     *
     * @var ProjectRepository
     */
    private $projectModel;

    /**
     *
     * @var ProjectValidator
     */
    private $projectValidator;

    public function __construct(ProjectRepository $model, ProjectValidator $validator)
    {
        $this->projectModel = $model;
        $this->projectValidator = $validator;
    }

    public function all()
    {
        return $this->projectModel->all();
    }

    public function find($id)
    {
        return $this->projectModel->find($id);
    }

    public function create(array $data)
    {
        try {
            $this->projectValidator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
            return $this->projectModel->create($data);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }
}
