<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProjectService;

class ProjectController extends Controller
{
    /**
     *
     * @var ProjectService
     */
    private $projectService;

    public function __construct(ProjectService $project)
    {
        $this->projectService = $project;
    }

    public function index()
    {
        return  $this->projectService->all();
    }

    public function show($id)
    {
        return $this->projectService->find($id);
    }

    public function store(Request $request)
    {
        return $this->projectService->create($request->all());
    }
}
