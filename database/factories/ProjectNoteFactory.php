<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\ProjectNote;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(ProjectNote::class, function (Faker $faker) {
    return [
        'project_id' => \rand(1, 10),
        'title' => $faker->word,
        'note' => $faker->paragraph
    ];
});
